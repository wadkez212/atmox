<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'atmox' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Vj5q,/KiS|g#sxx,|ej4`:dKiD1>jtVioXx,t:*Z!X.lYhQ~k|`$8`Ae;_umihvq' );
define( 'SECURE_AUTH_KEY',  'B)9ok%t#m2}d(2rjIKbPR+%ED%ArgaO0BQ],s_,mP1O!P[j0s[S9$c$HGF>&0/5t' );
define( 'LOGGED_IN_KEY',    'e#WnVN8>DsKYyi{]BEi|lC[.#a/z41*iJk!02u%p0`z}E49$iDP=va{z%RNmAMj$' );
define( 'NONCE_KEY',        'f/M=h0e{rtZ|<L2P@kp]cPrgO8C~!!||Y3=Po~p0KN`!y@v?eLEDPh(eV:n7g6:N' );
define( 'AUTH_SALT',        '76gIrm.[J/$VkQ(m@VV=i=[_w3Kmq}#aG4!0HeD~?ap/0VGkA/(g*$kK+oYZ=&!n' );
define( 'SECURE_AUTH_SALT', 'lThdP>qwx(8_Sf]_1pm-~iaP(]~uO=F=XePr$A8j4Lm`/>C/s]Y35x|+zt7KPrDT' );
define( 'LOGGED_IN_SALT',   '}mN(z/*i>ln7o9z!<fDb[5.+DO<#Kao0}B+6=#Z5#BT_Y$YsEz^WUEZBR%|~@k/|' );
define( 'NONCE_SALT',       'a-wa(z{5#KX1._T9lf4oO`UtWn;q+-{)@fPUnN-?Yn?{rLbcNJ 1_$&ZZ7Lh(k*(' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
